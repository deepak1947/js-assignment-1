// Take the commented ES5 syntax and convert it to ES6 arrow Syntax

// let myFunction = function () {};
const myFunction=()=>console.log("hi");
myFunction();

// let anotherFunction = function (param) {
//   return param;
// };
const anotherFunction=(a)=>a;
console.log(anotherFunction("hiii"));
console.log(anotherFunction(12345));

// let add = function (param1, param2) {
//   return param1 + param2;
// };
const add=(a,b)=>a+b;
// add(1,2);
console.log(add(1,2));

// let subtract = function (param1, param2) {
//   return param1 - param2;
// };
// subtract(1,2);
const subtract=(a,b)=>console.log(a-b);
subtract(1,2);
// exampleArray = [1,2,3,4];
// const triple = exampleArray.map(function (num) {
//   return num * 3;
// });
// console.log(triple);

var arr=[1,2,3,4,5];
const triple=arr.map((a)=>a*3);
console.log(triple);
